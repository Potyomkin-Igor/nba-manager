package com.nbamanager.web.player.controller;

import com.nbamanager.web.player.model.CreatePlayerRequest;
import com.nbamanager.web.player.service.PlayerWebService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequestMapping(value = "/players")
public class PlayerController {

    static final String CREATE_PLAYER_PAGE = "createPlayer";
    static final String INDEX_URL = "/";

    final PlayerWebService playerWebService;

    @GetMapping(value = "/create")
    public String getCreatePlayerPage(CreatePlayerRequest createPlayerRequest, Model model) {
        return CREATE_PLAYER_PAGE;
    }

    @PostMapping(value = "/create")
    public ModelAndView createPlayer(@Valid @ModelAttribute CreatePlayerRequest request,
                                     BindingResult bindingResult,
                                     ModelAndView model) {
        if(bindingResult.hasErrors()) {
            model.setViewName(CREATE_PLAYER_PAGE);
        } else {
            playerWebService.createPlayer(request);
            model.setView(redirectTo(INDEX_URL));
        }
        return model;
    }

    private RedirectView redirectTo(String url) {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance();
        return new RedirectView(
                builder
                        .path(url)
                        .build()
                        .toUriString()
        );
    }
}
