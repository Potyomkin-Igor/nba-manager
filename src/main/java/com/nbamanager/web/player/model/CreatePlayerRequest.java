package com.nbamanager.web.player.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CreatePlayerRequest {

    @NotBlank
    String firstName;

    @NotBlank
    String lastName;

    @NotBlank
    @Length(max = 15)
    String phone;

    @Length(max = 5)
    @NotBlank
    String height;

    @NotNull
    String teamId;
}
