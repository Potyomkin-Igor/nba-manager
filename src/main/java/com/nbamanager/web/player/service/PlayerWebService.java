package com.nbamanager.web.player.service;

import com.nbamanager.domain.model.Player;
import com.nbamanager.domain.model.Team;
import com.nbamanager.domain.service.TeamService;
import com.nbamanager.web.player.model.CreatePlayerRequest;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlayerWebService {

    final TeamService teamService;

    @Transactional
    public void createPlayer(CreatePlayerRequest request) {

        Player player = new Player();
        player.setFirstName(request.getFirstName());
        player.setLastName(request.getLastName());
        player.setDisplayName(getFullMame(request.getFirstName(), request.getLastName()));
        player.setPhone(request.getPhone());
        player.setHeightCm(Float.valueOf(request.getHeight()));

        Team team = teamService.getById(request.getTeamId());
        team.getPlayers().add(player);
        teamService.update(team);
        log.info("Player name: {}, have been created successfully", player.getDisplayName());
    }

    private String getFullMame(String firstName, String lastName) {
        return firstName.concat(" ").concat(lastName);
    }

}
