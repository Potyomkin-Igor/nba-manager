package com.nbamanager.web.team.model;

import com.nbamanager.web.player.model.PlayerDTO;
import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamDTO {

    String teamId;
    String name;
    List<PlayerDTO> players;
}
