package com.nbamanager.web.team.controller;

import com.nbamanager.web.team.service.TeamWebService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequestMapping(value = "/")
public class TeamController {

    static final String INDEX_PAGE = "index";

    final TeamWebService teamWebService;

    @GetMapping
    public String getAllTeams(Model model) {
        model.addAttribute("teams", teamWebService.getAllTeams());
        return INDEX_PAGE;
    }
}

