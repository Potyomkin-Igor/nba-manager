package com.nbamanager.web.team.service;

import com.nbamanager.domain.model.Player;
import com.nbamanager.domain.model.Team;
import com.nbamanager.domain.service.TeamService;
import com.nbamanager.web.player.model.PlayerDTO;
import com.nbamanager.web.team.model.TeamDTO;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamWebService {

    TeamService teamService;

    @Transactional(readOnly = true)
    public List<TeamDTO> getAllTeams() {
        List<Team> teams = teamService.findAll();
        return mapToTeamDTO(teams);
    }

    @Transactional
    public void createTeams(List<Team> teams) {
        if (CollectionUtils.isNotEmpty(teams)) {
            teams.forEach(team -> teamService.create(team));
        }
    }

    private List<TeamDTO> mapToTeamDTO(List<Team> teams) {
        return teams.stream()
                .map(team ->
                        TeamDTO.builder()
                                .teamId(team.getTeamId())
                                .name(team.getFullName())
                                .players(mapToPlayerDTO(team.getPlayers()))
                                .build()
                )
                .collect(Collectors.toList());
    }

    private List<PlayerDTO> mapToPlayerDTO(List<Player> players) {
        return players.stream()
                .map(player ->
                        PlayerDTO.builder()
                                .fullName(player.getDisplayName())
                                .height(player.getHeightCm())
                                .phone(player.getPhone())
                                .build()
                )
                .collect(Collectors.toList());
    }

}
