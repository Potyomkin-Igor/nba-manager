package com.nbamanager.domain.service.base;

import java.util.List;

/**
 * Defines commonly used CRUD operations
 */
public interface TeamCrudSupport<E> {

    /**
     * Retrieves an entity by its id.
     */
    E getById(final String entityId);

    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    List<E> findAll();

    /**
     * Updates an entity.
     *
     * @param entity entity to update
     * @return updated entity
     * @throws IllegalArgumentException if entity hasn't persisted yet
     */
    E update(final E entity);

    /**
     * Persists an entity.
     *
     * @param entity entity to create
     * @return created entity
     * @throws IllegalArgumentException if entity has already persisted
     */
    E create(final E entity);

    /**
     * Removes an entity.
     *
     * @param entity entity to remove
     * @throws IllegalArgumentException if entity hasn't persisted yet
     */
    void delete(final E entity);

    /**
     * Removes an entity by it's identifier.
     *
     * @param id unique identifier
     */
    void delete(final String id);


}
