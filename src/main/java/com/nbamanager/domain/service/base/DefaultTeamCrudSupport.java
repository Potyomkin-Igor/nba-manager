package com.nbamanager.domain.service.base;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Default implementation of {@link TeamCrudSupport} which simply delegates
 * CRUD operations to {@link CrudRepository}.
 */
@AllArgsConstructor
public abstract class DefaultTeamCrudSupport<E> implements TeamCrudSupport<E>{

    private CrudRepository<E, String> repository;

    @Override
    public E getById(String entityId) {
        return repository
                .findById(entityId)
                .orElseThrow(() -> new EmptyResultDataAccessException("Entity was not found by ID: " + entityId, 1));
    }

    @Override
    public List<E> findAll() {
      return Lists.newArrayList(repository.findAll());
    }

    @Override
    public E update(E entity) {
        return repository.save(entity);
    }

    @Override
    public E create(E entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(E entity) {
        repository.delete(entity);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
