package com.nbamanager.domain.service;

import com.nbamanager.domain.model.Team;
import com.nbamanager.domain.repository.TeamRepository;
import com.nbamanager.domain.service.base.DefaultTeamCrudSupport;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeamService extends DefaultTeamCrudSupport<Team> {

    final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        super(teamRepository);
        this.teamRepository = teamRepository;
    }

    @Transactional(readOnly = true)
    public List<Team> getFirstSixWithoutPlayers() {
        return teamRepository.findFirst6ByPlayersIsNull();
    }
}
