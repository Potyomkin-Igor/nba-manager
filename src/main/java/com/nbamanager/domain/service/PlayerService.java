package com.nbamanager.domain.service;

import com.nbamanager.domain.model.Player;
import com.nbamanager.domain.repository.PlayerRepository;
import com.nbamanager.domain.service.base.DefaultPLayerCrudSupport;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlayerService extends DefaultPLayerCrudSupport<Player> {

    final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        super(playerRepository);
        this.playerRepository = playerRepository;
    }
}
