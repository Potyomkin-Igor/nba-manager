package com.nbamanager.domain.repository;

import com.nbamanager.domain.model.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * DAO layer for {@link Team}
 */
public interface TeamRepository extends CrudRepository<Team, String> {

    List<Team> findFirst6ByPlayersIsNull();
}
