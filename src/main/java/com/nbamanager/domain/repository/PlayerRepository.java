package com.nbamanager.domain.repository;

import com.nbamanager.domain.model.Player;
import org.springframework.data.repository.CrudRepository;

/**
 * DAO layer for {@link Player}
 */
public interface PlayerRepository extends CrudRepository<Player, Long> {
}
