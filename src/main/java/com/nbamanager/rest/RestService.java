package com.nbamanager.rest;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nbamanager.domain.model.Player;
import com.nbamanager.domain.model.Team;
import com.nbamanager.domain.service.TeamService;
import com.nbamanager.hibernate.NbaTransactionalManager;
import com.nbamanager.web.team.service.TeamWebService;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RestService {

    static final String TEAMS_URL = "https://erikberg.com/nba/teams.json";
    static final String BEARER_AUTH_TOKEN = "Bearer %s";
    static final String PLAYERS_URL = "https://erikberg.com/nba/roster/%s.json";

    ObjectMapper objectMapper;
    RestTemplate restTemplate;
    TeamService teamService;
    TeamWebService teamWebService;
    NbaTransactionalManager nbaTransactionalManager;

    @Value("${api.authorization.token}")
    String token;

    public RestService(ObjectMapper objectMapper, RestTemplate restTemplate,
                       TeamService teamService, TeamWebService teamWebService,
                       NbaTransactionalManager nbaTransactionalManager) {
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.teamService = teamService;
        this.teamWebService = teamWebService;
        this.nbaTransactionalManager = nbaTransactionalManager;
    }

    /**
     * This method will execute only once at application startup.
     *
     * Create request to external API to retrieve {@link Team}s,
     * and then call {@code getPlayersPerCommand()} method to retrieve {@link Player}s
     * for first six teams without players.
     */
    public void callForTeams() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(TEAMS_URL);
        ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

        List<Team> teams = HttpStatus.OK.equals(response.getStatusCode())
                ? getTeamsFromResponse(response.getBody())
                : Collections.emptyList();

        teamWebService.createTeams(teams);
        getPlayersPerCommand();
    }

    /**
     * Scheduled method, which will execute every a minute,
     * take from database first six {@link Team}s without {@link Player}s,
     * and make request to external API for retrieving players for each team.
     *
     * {@code (fixedDelay = 60000)} - Duration between the end of last execution
     * and the start of next execution is fixed.
     * The task always waits until the previous one is finished.
     */
    @Scheduled(initialDelay = 0, fixedDelay = 60000)
    private void getPlayersPerCommand() {
        List<Team> teams = teamService.getFirstSixWithoutPlayers();

        nbaTransactionalManager.executeInWriteTransaction(status -> {
            if (CollectionUtils.isNotEmpty(teams)) {
                teams.forEach(team -> {
                    List<Player> players = callForPlayers(team.getTeamId());
                    team.getPlayers().addAll(players);
                    teamService.update(team);
                });
            }
            return null;
        });
    }

    /**
     * Request to external API for withdraw {@link List<Player>} per {@link Team}
     *
     * @param teamId {@link Team} id
     */
    private List<Player> callForPlayers(String teamId) {
        String accessToken = String.format(BEARER_AUTH_TOKEN, decodeToken(token));
        String urlForPlayers = String.format(PLAYERS_URL, teamId);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, accessToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                urlForPlayers, HttpMethod.GET, entity, String.class
        );

        return HttpStatus.OK.equals(response.getStatusCode())
                ? getPlayersFromResponse(response.getBody())
                : Collections.emptyList();
    }

    @SneakyThrows
    private List<Team> getTeamsFromResponse(String body) {
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, Team.class);
        return objectMapper.readValue(body, type);
    }

    @SneakyThrows
    private List<Player> getPlayersFromResponse(String body) {
        ObjectNode node = (ObjectNode) objectMapper.readTree(body);
        JsonNode jsonNode = node.get("players");
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, Player.class);
        return objectMapper.readValue(jsonNode.toString(), type);
    }

    /**
     * @param token encoded token
     */
    private String decodeToken(String token) {
        byte[] decodedPassword = Base64.decodeBase64(token.getBytes());
        return new String(decodedPassword);
    }
}
