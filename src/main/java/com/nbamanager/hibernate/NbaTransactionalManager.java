package com.nbamanager.hibernate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Was done to provides a way to incapsulate methods,
 * because {@link org.springframework.transaction.annotation.Transactional} requires public access level.
 */
@Component
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NbaTransactionalManager {

    JpaTransactionManager jpaTransactionManager;

    public <T> T executeInWriteTransaction(TransactionCallback<T> transactionCallback) {
        TransactionTemplate transactionTemplate = new TransactionTemplate(jpaTransactionManager);
        return transactionTemplate.execute(transactionCallback);
    }
}
